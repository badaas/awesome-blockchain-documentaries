# awesome-blockchain-documentaries

A curated list of awesome blockchain related documentaries and film makers

## Note

Please support as much as you can the producers.

## Film makers and their production

- Torsten Hoffmann
  - [Cryptopia](https://www.youtube.com/watch?v=M6GgU08rxyg)
  - [Bitcoin: The End of Money as We Know It](https://vimeo.com/ondemand/bitcoin)

## YouTube channels

## General videos about blockchain and DLT
